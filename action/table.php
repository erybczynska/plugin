<?php

require_once 'class.user.php';

function showUserTable($users, Doku_Event $event)
{

    echo("<script>console.log($event->data);</script>");

    $event->data .= '<table>' .
        '<tr>' .
        '<th></th>' .
        '<th>User</th>' .
        '<th>Editions</th>' .
        '<th>Creations</th>' .
        '<th>Deleted</th>' .
        '<th>Weighted Average</th>' .
        '<th>Test passed</th>' .
        '<th>Events</th>' .
        '<th>Result</th>' .
        '</tr>';

    $position = 1;

    foreach ($users as &$user) {
        $weightedAverage = calculateWeightedAverage($user->getWeightedAverage());

        $event->data .= '<tr>' .
            '<td>' . $position . '</td>' .
            '<td>' . $user->getName() . '</td>' .
            '<td>' . $user->getEdited() . '</td>' .
            '<td>' . $user->getCreated() . '</td>' .
            '<td>' . $user->getDeleted() . '</td>' .
            '<td>' . $weightedAverage . '</td>' .
            '<td>' . $user->getTestPassedMark() . '</td>' .
            '<td>' . $user->getEventsMark() . '</td>' .
            '<td>' . $user->getMeanValue() . '</td>' .
            '</tr>';
        $position++;

    }
    $event->data .= '</table>';
}

