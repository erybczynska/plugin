<?php

function normalizeValue($val)
{
    $max = 100;
    $newMax = 5;

    $newValue = $val / $max * $newMax;
    return round($newValue, 2);
}

function calculateWeightedAverage($array)
{
    return round(array_sum($array) / sizeof($array), 2);
}

function calculateMeanValue($events, $tests, $weightedAverage)
{
    return round(($events + $tests + $weightedAverage) / 3, 2);
}
