<?php

const EVENT = 'lokievent:([a-z]+)_[a-z0-9_]+ a prov:Activity ;\s+';
const USER = 'prov:wasAssociatedWith lokiuser:(\S+) ;\s+';
const DESCRIPTION = 'dc:description "\w+" ;\s+';
const WHAT_WAS_DONE =  'loki:whatWasDone "[\w\s]+" ;\s+';
const WHY_WAS_DONE = 'loki:whyWasDone "[\w\s]+" ;\s+';
const CLASSES_CHANGE = 'prov:used [\w\s,]+;\s+loki:classesChange "\d";\s+';
const RELATIONS_CHANGE = 'loki:relationsChange "\d";\s+';
const ATTRIBUTES_CHANGE = 'loki:attributesChange "\d";\s+';
const STATEMENTS_CHANGE = 'loki:statementsChange "\d";\s+';
const TEST_PASSED = 'loki:testsPassed \[ loki:valueBefore "(\d+)" ; loki:valueAfter "(\d+)" \] ;\s+';
const ATTRIBUTE_RICHNESS = 'loki:AttributeRichness \[ loki:valueBefore "\d"; loki:valueAfter "\d" \] ;\s+';
const AVERAGE_POPULATION = 'loki:AveragePopulation \[ loki:valueBefore "\d"; loki:valueAfter "\d" \] ;\s+';
const SIZE_OF_VOCABULARY = 'loki:SizeOfVocabulary \[ loki:valueBefore "\d"; loki:valueAfter "\d" \] ;\s+';
const EDGE_NODE_RATIO = 'loki:EdgeNodeRatio \[ loki:valueBefore "\d"; loki:valueAfter "\d" \] ;\s+';
const WEIGHTED_AVERAGE = 'loki:weightedAverage "(\d+\.?\d*)" ;';

define ("USER_PATTERN", '/' . EVENT . USER . DESCRIPTION . WHAT_WAS_DONE . WHY_WAS_DONE . CLASSES_CHANGE
    . RELATIONS_CHANGE . ATTRIBUTES_CHANGE . STATEMENTS_CHANGE . TEST_PASSED . ATTRIBUTE_RICHNESS
    . AVERAGE_POPULATION . SIZE_OF_VOCABULARY . EDGE_NODE_RATIO . WEIGHTED_AVERAGE . '/');