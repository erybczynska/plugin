<?php

if (!defined('DOKU_INC')) die();

require_once 'class.user.php';
require_once 'pattern.php';

class action_plugin_wikigame_wikigame extends DokuWiki_Action_Plugin
{
    const PATH = 'data/prov/';

    public function register(Doku_Event_Handler $controller)
    {
        $controller->register_hook('TPL_CONTENT_DISPLAY', 'BEFORE', $this, 'insert_user_list', array());
    }

    public function insert_user_list(Doku_Event $event, $param)
    {
        $users = $this->getUsers(self::PATH);
        usort($users, array($this, 'sortByMeanValue'));
        showUserTable($users, $event);
    }

    private function getUsers($path)
    {

        $users = array();

        foreach (scandir($path) as $file) {
            if ($file == '.' || $file == '..')
                continue;
            else if (is_dir($path . $file)) {
                $dirPath = $path . $file . '/';
                $dirUsers = $this->getUsers($dirPath);

                foreach ($dirUsers as &$user) {
                    $userName = $user->getName();
                    $dirUser = $dirUsers[$userName];

                    $created = $dirUser->getCreated();
                    $edited = $dirUser->getEdited();
                    $deleted = $dirUser->getDeleted();
                    $weightedAverage = $dirUser->getWeightedAverage();
                    $testPassed = $dirUser->getTestPassed();

                    if (array_key_exists($userName, $users)) {
                        $user = $users[$userName];
                        $user->updateUser($created, $edited, $deleted, $weightedAverage, $testPassed);
                    } else {
                        $newUser = User::newUser($userName, $created, $edited, $deleted, $weightedAverage, $testPassed);
                        $users[$userName] = $newUser;
                    }
                }
            } else {
                $pathName = $path . $file;
                $fileContent = file_get_contents($pathName, true);
                $pattern = USER_PATTERN;
                preg_match_all($pattern, $fileContent, $matches);

                $names = $matches[2];

                for ($i = 0; $i <= sizeof($names) - 1; $i++) {

                    $userName = $names[$i];
                    $testPassedValueBefore = $matches[3][$i];
                    $testPassedValueAfter = $matches[4][$i];
                    $testPassed = $testPassedValueAfter - $testPassedValueBefore;

                    $weightedAverage = $matches[5][$i];
                    $event = $matches[1][$i];

                    if (array_key_exists($userName, $users)) {
                        $currentUser = $users[$userName];
                        $currentUser->updateUserWithEvent($event, $weightedAverage, $testPassed);
                    } else {
                        $newUser = User::withEvent($event, $userName, $weightedAverage, $testPassed);
                        $users[$userName] = $newUser;
                    }
                }
            }
        }

        if (sizeof($users) > 0) {
            $bestEvents = $this->getBestEventNumber($users);
            $bestTestPassed = $this->getBestTestPassed($users);

            foreach ($users as &$user) {
                $user->setEventsMark($bestEvents);
                $user->setTestPassedMark($bestTestPassed);
                $user->setMeanValue();
            }
        }
        return $users;
    }

    function getBestEventNumber($users)
    {
        return max(array_map(function ($user) {
            return $user->getEvents();
        }, $users));
    }

    function getBestTestPassed($users)
    {
        return max(array_map(function ($user) {
            return $user->getTestPassed();
        }, $users));
    }

    private function sortByMeanValue($a, $b)
    {
        return $a->getMeanValue() < $b->getMeanValue();
    }

}