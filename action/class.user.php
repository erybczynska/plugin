<?php
require_once('events.php');

class User
{
    private $name;
    private $created;
    private $edited;
    private $deleted;
    private $weightedAverage;
    private $testPassed;

    private $eventsMark;
    private $testPassedMark;
    private $meanValue;

    function __construct($name, $weightedAverage, $testPassed)
    {
        $this->name = $name;
        $this->edited = 0;
        $this->created = 0;
        $this->deleted = 0;
        $this->weightedAverage = array();
        $this->updateWeightedAverage($weightedAverage);
        $this->testPassed = $testPassed;


    }

    public static function newUser($name, $created, $edited, $deleted, $weightedAverage, $testPassed)
    {
        $instance = new self($name, $weightedAverage, $testPassed);
        $instance->setEvents($created, $edited, $deleted);
        return $instance;
    }

    public static function withEvent($event, $name, $weightedAverage, $testPassed)
    {
        $instance = new self($name, $weightedAverage, $testPassed);
        $instance->updateSingleEvent($event);
        return $instance;
    }

    public function updateUser($created, $edited, $deleted, $weightedAverage, $testPassed)
    {
        $this->updateTestPassed($testPassed);
        $this->updateWeightedAverage($weightedAverage);
        $this->updateEvents($created, $edited, $deleted);

    }

    public function updateUserWithEvent($event, $weightedAverage, $testPassed)
    {
        $this->updateSingleEvent($event);
        $this->updateTestPassed($testPassed);
        $this->updateWeightedAverage($weightedAverage);
    }

    private function updateWeightedAverage($weightedAverage)
    {
        if (is_array($weightedAverage)) {
            foreach ($weightedAverage as &$val) {
                array_push($this->weightedAverage, $val);
            }
        } else {
            array_push($this->weightedAverage, $weightedAverage);
        }
    }

    private function updateSingleEvent($event) {
        switch ($event) {
            case CREATED:
                $this->created += 1;
                break;
            case EDITED:
                $this->edited += 1;
                break;
            case DELETED:
                $this->deleted += 1;
                break;
        }
    }

    private function setEvents($created, $edited, $deleted) {
        $this->created = $created;
        $this->edited = $edited;
        $this->deleted = $deleted;
    }

    private function updateEvents($created, $edited, $deleted) {
        $this->created += $created;
        $this->edited += $edited;
        $this->deleted += $deleted;
    }

    private function updateTestPassed($testPassed)
    {
        $this->testPassed += $testPassed;
    }
    public function getName()
    {
        return $this->name;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getEdited()
    {
        return $this->edited;
    }

    public function getDeleted()
    {
        return $this->deleted;
    }

    public function getEvents() {
        return $this->created + $this->edited + $this->deleted;
    }

    public function getWeightedAverage()
    {
        return $this->weightedAverage;
    }

    public function getTestPassed()
    {
        return $this->testPassed;
    }
    public function getEventsMark()
    {
        return $this->eventsMark;
    }

    public function getTestPassedMark()
    {
        return $this->testPassedMark;
    }
    public function getMeanValue()
    {
        return $this->meanValue;
    }
    public function setEventsMark($bestEvents) {
        $userEvents = $this->getEvents();
        $this->eventsMark = normalizeValue($userEvents / $bestEvents * 100);

    }
    public function setTestPassedMark($bestTestPassed) {
        $userTests = $this->getTestPassed();
        $this->testPassedMark = normalizeValue($userTests / $bestTestPassed * 100);
    }
    public function setMeanValue() {
        $events = $this->getEventsMark();
        $tests = $this->getTestPassedMark();
        $weightedAverage = calculateWeightedAverage($this->getWeightedAverage());
        $this->meanValue = calculateMeanValue($events, $tests, $weightedAverage);
    }
}
